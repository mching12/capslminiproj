package com.avocadoapps.capslminiproject.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.avocadoapps.capslminiproject.R
import com.avocadoapps.capslminiproject.data.model.Game
import com.avocadoapps.capslminiproject.view.listener.OnGameSelectListener
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.card_details.view.*

class GameGridAdapter constructor(
    private val gameSelectListener: OnGameSelectListener,
    private val gameList: ArrayList<Game>
) : RecyclerView.Adapter<GameGridAdapter.GameViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_details, parent, false)
        return GameViewHolder(view)
    }

    override fun onBindViewHolder(holder: GameViewHolder, position: Int) {
        holder.bind(gameList[position], gameSelectListener)
    }

    override fun getItemCount(): Int {
        return gameList.size
    }

    class GameViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(game: Game, gameSelectListener: OnGameSelectListener) {
            loadImage(itemView.ivCardImage, game)
            itemView.tvCardLabel.text = game.name
            itemView.card.setOnClickListener {
                gameSelectListener.onSelected(game)
            }
        }

        private fun loadImage(image: ImageView, game: Game) {
            if (game.image != null) {
                Glide.with(image.context)
                    .load(game.image)
                    .apply(RequestOptions.placeholderOf(android.R.color.darker_gray))
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(image)
            }
        }
    }
}
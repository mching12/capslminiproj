package com.avocadoapps.capslminiproject.view.listener

import com.avocadoapps.capslminiproject.data.model.Stage

interface OnStageSelectListener {
    fun onSelected(stage: Stage)
}
package com.avocadoapps.capslminiproject.view.listener

import com.avocadoapps.capslminiproject.data.model.Game

interface OnGameSelectListener {
    fun onSelected(game: Game)
}
package com.avocadoapps.capslminiproject.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.avocadoapps.capslminiproject.R
import com.avocadoapps.capslminiproject.data.model.Stage
import com.avocadoapps.capslminiproject.view.listener.OnStageSelectListener
import kotlinx.android.synthetic.main.card_details.view.*

class StageGridAdapter constructor(
    private val stageSelectListener: OnStageSelectListener,
    private val stagelist: ArrayList<Stage>
) : RecyclerView.Adapter<StageGridAdapter.StageViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StageViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_details, parent, false)
        return StageViewHolder(view)
    }

    override fun onBindViewHolder(holder: StageViewHolder, position: Int) {
        holder.bind(stagelist[position], stageSelectListener)
    }

    override fun getItemCount(): Int {
        return stagelist.size
    }

    class StageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(stage: Stage, stageSelectListener: OnStageSelectListener) {
            loadImage(itemView.ivCardImage, stage)
            itemView.tvCardLabel.text = stage.name
            itemView.card.setOnClickListener {
                stageSelectListener.onSelected(stage)
            }
        }

        private fun loadImage(image: ImageView, stage: Stage) {
            val resource =
                if(stage.isActive) stage.getStageType().activeResource
                else stage.getStageType().inactiveResource
            image.setImageResource(resource)
        }
    }
}
package com.avocadoapps.capslminiproject.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.avocadoapps.capslminiproject.R
import kotlinx.android.synthetic.main.layout_tag.view.*


class TagAdapter constructor(
    private val taglist: ArrayList<String>
) : RecyclerView.Adapter<TagAdapter.TagViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TagViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.layout_tag, parent, false)
        return TagViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: TagViewHolder, position: Int) {
        holder.bind(taglist[position])
    }

    override fun getItemCount(): Int {
        return taglist.size
    }

    class TagViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(label: String) {
            itemView.tvTagLabel.text = label
        }
    }
}
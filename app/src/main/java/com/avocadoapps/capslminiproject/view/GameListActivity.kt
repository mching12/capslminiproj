package com.avocadoapps.capslminiproject.view

import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.avocadoapps.capslminiproject.R
import kotlinx.android.synthetic.main.layout_toolbar.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.avocadoapps.capslminiproject.data.model.Game
import com.avocadoapps.capslminiproject.data.state.GameListState
import com.avocadoapps.capslminiproject.view.adapter.GameGridAdapter
import com.avocadoapps.capslminiproject.view.listener.OnGameSelectListener
import com.avocadoapps.capslminiproject.viewmodel.GameListViewmodel
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_game_list.*
import javax.inject.Inject

class GameListActivity : BaseActivity(), OnGameSelectListener, SwipeRefreshLayout.OnRefreshListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var gameListViewmodel: GameListViewmodel

    //  PR1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_list)
        AndroidInjection.inject(this)
        initUi()
        initVm()
    }

    private fun initVm() {
        gameListViewmodel = ViewModelProvider(this,
            viewModelFactory)[GameListViewmodel::class.java].apply {
            getGameListState().observe(this@GameListActivity, Observer {
                renderGameListState(it)
            })
        }
    }

    private fun initUi() {
        setupToolbar()
        refreshLayout.setOnRefreshListener(this)
        displayGames()
    }

    private fun renderGameListState(gameListState: GameListState) {
        refreshLayout.isRefreshing = gameListState is GameListState.InProgress
        when(gameListState) {
            is GameListState.Success -> displayGames(gameListState.gameList)
            is GameListState.Error -> {
                showSnackbar(parentLayout,
                    if(gameListState.error.isNullOrEmpty())
                        getString(R.string.error_generic)
                    else gameListState.error)
            }
        }
    }

    private fun displayGames(gameList: ArrayList<Game>? = null) {
        rvGames.apply {
            layoutManager = GridLayoutManager(this@GameListActivity, NO_OF_COLUMNS)
            setHasFixedSize(true)
            adapter = GameGridAdapter(this@GameListActivity,
                gameList ?: arrayListOf())
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        tvToolbarTitle.text = getString(R.string.title_choose_game)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onSelected(game: Game) {
        StageListActivity.launch(this@GameListActivity, game)
    }

    override fun onRefresh() {
        gameListViewmodel.fetchGameList()
    }

    override fun onResume() {
        super.onResume()
        gameListViewmodel.fetchGameList()
    }

    companion object {

        private const val NO_OF_COLUMNS = 2

    }
}

package com.avocadoapps.capslminiproject.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.layout_toolbar.*
import androidx.recyclerview.widget.GridLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.avocadoapps.capslminiproject.R
import com.avocadoapps.capslminiproject.data.model.Game
import com.avocadoapps.capslminiproject.data.model.Stage
import com.avocadoapps.capslminiproject.data.state.StageListState
import com.avocadoapps.capslminiproject.view.adapter.StageGridAdapter
import com.avocadoapps.capslminiproject.view.listener.OnStageSelectListener
import com.avocadoapps.capslminiproject.viewmodel.StageListViewmodel
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_stage_list.*
import javax.inject.Inject
import com.google.android.flexbox.JustifyContent
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexboxLayoutManager
import com.avocadoapps.capslminiproject.view.adapter.TagAdapter
import com.google.android.flexbox.FlexWrap


class StageListActivity : BaseActivity(), OnStageSelectListener, SwipeRefreshLayout.OnRefreshListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private lateinit var stageListViewmodel: StageListViewmodel

    //  PR2
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_stage_list)
        AndroidInjection.inject(this)
        initUi()
        initVm()
    }

    private fun initVm() {
        stageListViewmodel = ViewModelProvider(this,
            viewModelFactory)[StageListViewmodel::class.java].apply {
            getStageListState().observe(this@StageListActivity, Observer {
                renderStageListState(it)
            })
        }
    }

    private fun initUi() {
        val game = intent.getParcelableExtra<Game>(EXTRA_GAME)
        val tags = arrayListOf(game?.name ?: "")
        setupToolbar()
        refreshLayout.setOnRefreshListener(this)
        displayStages()
        displayTags(tags)
    }

    private fun renderStageListState(stageListState: StageListState) {
        refreshLayout.isRefreshing = stageListState is StageListState.InProgress
        when(stageListState) {
            is StageListState.Success -> displayStages(stageListState.stageList)
            is StageListState.Error -> {
                showSnackbar(parentLayout,
                    if(stageListState.error.isNullOrEmpty())
                        getString(R.string.error_generic)
                    else stageListState.error)
            }
        }
    }

    private fun displayStages(stageList: ArrayList<Stage>? = null) {
        rvStages.apply {
            layoutManager = GridLayoutManager(this@StageListActivity, NO_OF_COLUMNS)
            setHasFixedSize(true)
            adapter = StageGridAdapter(this@StageListActivity,
                stageList ?: arrayListOf())
        }
    }

    private fun displayTags(taglist: ArrayList<String>) {
        val flexLayoutManager = FlexboxLayoutManager(this).apply {
            flexWrap = FlexWrap.WRAP
            flexDirection = FlexDirection.ROW
            justifyContent = JustifyContent.FLEX_START
        }
        rvTags.apply {
            layoutManager = flexLayoutManager
            adapter = TagAdapter(taglist)
        }
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)
        tvToolbarTitle.text = getString(R.string.title_tournament_info)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onSelected(stage: Stage) {
        //  Launch next activity
    }

    override fun onRefresh() {
        stageListViewmodel.fetchStageList()
    }

    override fun onResume() {
        super.onResume()
        stageListViewmodel.fetchStageList()
    }

    companion object {

        private const val EXTRA_GAME = "extra_game"
        private const val NO_OF_COLUMNS = 2

        fun launch(activity: Activity, game: Game) {
            val intent = Intent(activity, StageListActivity::class.java).apply {
                putExtra(EXTRA_GAME, game)
            }
            activity.startActivity(intent)
        }
    }
}

package com.avocadoapps.capslminiproject.data.state

import com.avocadoapps.capslminiproject.data.model.Stage

sealed class StageListState {
    object InProgress: StageListState()
    data class Success(val stageList: ArrayList<Stage>?): StageListState()
    data class Error(val error: String?): StageListState()
}
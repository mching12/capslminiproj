package com.avocadoapps.capslminiproject.data.model

import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Game(
    @Expose
    var id: Int = 0,
    @Expose
    var name: String? = null,
    @Expose
    var image: String? = null,
    @Expose @SerializedName("package_id")
    var packageId: String? = null
) : Parcelable
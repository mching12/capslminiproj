package com.avocadoapps.capslminiproject.data

import com.avocadoapps.capslminiproject.data.model.GameListPageResult
import com.avocadoapps.capslminiproject.data.model.StageListPageResult
import com.avocadoapps.capslminiproject.network.APIServiceGenerator
import com.avocadoapps.capslminiproject.network.GameApiService
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GameRepository @Inject constructor() {

    fun fetchGameList(limit: Int? = null, offset: Int? = null): Observable<Response<GameListPageResult>> {
        return APIServiceGenerator()
            .createService(GameApiService::class.java)
            .getGames(limit, offset)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun fetchStageList(limit: Int? = null, offset: Int? = null): Observable<Response<StageListPageResult>> {
        return APIServiceGenerator()
            .createService(GameApiService::class.java)
            .getStages(limit, offset)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

}
package com.avocadoapps.capslminiproject.data.state

import com.avocadoapps.capslminiproject.data.model.Game

sealed class GameListState {
    object InProgress: GameListState()
    data class Success(val gameList: ArrayList<Game>?): GameListState()
    data class Error(val error: String?): GameListState()
}
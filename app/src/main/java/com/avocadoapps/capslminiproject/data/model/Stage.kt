package com.avocadoapps.capslminiproject.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Stage(
    @Expose
    var id: Int = 0,
    @Expose
    var name: String? = null,
    @Expose @SerializedName("is_active")
    var isActive: Boolean = false
) {
    fun getStageType(): StageType {
        return StageType.valueOf(id)
    }
}


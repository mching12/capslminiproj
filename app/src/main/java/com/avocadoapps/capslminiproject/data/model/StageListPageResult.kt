package com.avocadoapps.capslminiproject.data.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.ArrayList

data class StageListPageResult(
    @Expose
    var count: Int = 0,
    @Expose
    var next: Int? = null,
    @Expose
    var previous: Int? = null,
    @Expose @SerializedName("results")
    var stageList: ArrayList<Stage>? = null
)
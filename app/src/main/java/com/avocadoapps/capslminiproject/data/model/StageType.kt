package com.avocadoapps.capslminiproject.data.model

import com.avocadoapps.capslminiproject.R
import java.util.*
import java.util.stream.Collectors


enum class StageType(val id: Int, val activeResource: Int, val inactiveResource: Int) {
    UNKNOWN(0, R.drawable.ic_block_black_24dp, R.drawable.ic_block_black_24dp),
    LEADERBOARD(1, R.drawable.ic_format_list_numbered_black_24dp , R.drawable.ic_block_black_24dp),
    SINGLE_ELIM(2, R.drawable.ic_accessibility_black_24dp, R.drawable.ic_block_black_24dp),
    SIMPLE(3, R.drawable.ic_android_black_24dp, R.drawable.ic_block_black_24dp),
    ROUND_ROBIN(4, R.drawable.ic_subdirectory_arrow_right_black_24dp, R.drawable.ic_block_black_24dp),
    DOUBLE_ELIM(5, R.drawable.ic_group_black_24dp, R.drawable.ic_block_black_24dp);

    companion object {

        private val map: Map<Int, StageType>

        init {
            map = Arrays.stream(values())
                .collect(Collectors.toMap({ e -> e.id }, { e -> e }))
        }

        fun valueOf(id: Int): StageType {
            return Optional.ofNullable(map[id]).orElse(UNKNOWN)
        }
    }
}

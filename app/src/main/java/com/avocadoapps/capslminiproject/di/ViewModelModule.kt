package com.avocadoapps.capslminiproject.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.avocadoapps.capslminiproject.viewmodel.GameListViewmodel
import com.avocadoapps.capslminiproject.viewmodel.StageListViewmodel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(GameListViewmodel::class)
    abstract fun bindGameListViewModel(gameListViewmodel: GameListViewmodel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(StageListViewmodel::class)
    abstract fun bindStageListViewModel(stageListViewmodel: StageListViewmodel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}

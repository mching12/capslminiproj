package com.avocadoapps.capslminiproject.di

import com.avocadoapps.capslminiproject.view.GameListActivity
import com.avocadoapps.capslminiproject.view.StageListActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuildersModule {
    @ContributesAndroidInjector
    abstract fun bindGameListActivity(): GameListActivity

    @ContributesAndroidInjector
    abstract fun bindStageListActivity(): StageListActivity
}
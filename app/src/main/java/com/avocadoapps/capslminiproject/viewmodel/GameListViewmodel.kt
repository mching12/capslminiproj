package com.avocadoapps.capslminiproject.viewmodel

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.avocadoapps.capslminiproject.data.GameRepository
import com.avocadoapps.capslminiproject.data.state.GameListState
import javax.inject.Inject

class GameListViewmodel @Inject constructor(
    private val gameRepository: GameRepository
): ViewModel() {

    private val _gameListState = MutableLiveData<GameListState>()

    fun getGameListState(): LiveData<GameListState> = _gameListState

    @SuppressLint("CheckResult")
    fun fetchGameList() {
        _gameListState.value = GameListState.InProgress
        gameRepository.fetchGameList()
            .subscribe ({
                if(it.isSuccessful && it.body() != null) {
                    _gameListState.value = GameListState.Success(it.body()!!.gameList)
                } else _gameListState.value = GameListState.Error(it.message())
            }, {
                _gameListState.value = GameListState.Error(it.localizedMessage)
            })
    }
}
package com.avocadoapps.capslminiproject.viewmodel

import android.annotation.SuppressLint
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.avocadoapps.capslminiproject.data.GameRepository
import com.avocadoapps.capslminiproject.data.state.StageListState
import javax.inject.Inject

class StageListViewmodel @Inject constructor(
    private val gameRepository: GameRepository
): ViewModel() {

    private val _stageListState = MutableLiveData<StageListState>()

    fun getStageListState(): LiveData<StageListState> = _stageListState

    @SuppressLint("CheckResult")
    fun fetchStageList() {
        _stageListState.value = StageListState.InProgress
        gameRepository.fetchStageList()
            .subscribe ({
                if(it.isSuccessful && it.body() != null) {
                    _stageListState.value = StageListState.Success(it.body()!!.stageList)
                } else _stageListState.value = StageListState.Error(it.message())
            }, {
                _stageListState.value = StageListState.Error(it.localizedMessage)
            })
    }
}
package com.avocadoapps.capslminiproject.network

import com.google.gson.Gson
import com.google.gson.GsonBuilder

class GsonModule private constructor() {

    val gson: Gson = GsonBuilder()
        .excludeFieldsWithoutExposeAnnotation()
        .create()

    companion object {
        private val mutex = Any()
        private var ourInstance: GsonModule? = GsonModule()

        val instance: GsonModule
            get() {
                if (ourInstance == null) {
                    synchronized(mutex) {
                        if (ourInstance == null) ourInstance = GsonModule()
                    }
                }
                return ourInstance!!
            }
    }
}
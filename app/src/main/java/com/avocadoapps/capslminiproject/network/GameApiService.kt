package com.avocadoapps.capslminiproject.network

import com.avocadoapps.capslminiproject.data.model.GameListPageResult
import com.avocadoapps.capslminiproject.data.model.StageListPageResult
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*

interface GameApiService {

    @GET("/api/games/")
    fun getGames(
        @Query("limit") limit: Int?,
        @Query("offset") offset: Int?
    ): Observable<Response<GameListPageResult>>

    @GET("/api/stages/")
    fun getStages(
        @Query("limit") limit: Int?,
        @Query("offset") offset: Int?
    ): Observable<Response<StageListPageResult>>

}
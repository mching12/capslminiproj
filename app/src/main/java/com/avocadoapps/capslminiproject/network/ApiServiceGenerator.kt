package com.avocadoapps.capslminiproject.network

import com.avocadoapps.capslminiproject.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class APIServiceGenerator {

    private var retrofit: Retrofit? = null

    init {
        createClient(BuildConfig.BASE_URL)
    }

    private fun createClient(host: String) {
        val httpClient = OkHttpClient.Builder()
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        httpClient.addInterceptor(logging)
        val client = httpClient.build()

        retrofit = Retrofit.Builder()
            .baseUrl(host)
            .addConverterFactory(GsonConverterFactory.create(GsonModule.instance.gson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(client)
            .build()
    }

    fun <T> createService(service: Class<T>): T {
        return retrofit!!.create(service)
    }
}